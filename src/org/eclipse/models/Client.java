package org.eclipse.models;

public class Client {

	private int num;
	private String nom;
	private String prenom;
	private String mail;

	
	public Client(int num, String nom, String prenom, String mail) {
		super();
		this.num = num;
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		
	}
	public Client() {
		super();
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	@Override
	public String toString() {
		return "Client [num=" + num + ", nom=" + nom + ", prenom=" + prenom + ", mail=" + mail + "]";
	}

	


	
	
	
	

	
	
}
