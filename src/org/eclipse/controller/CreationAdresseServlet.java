package org.eclipse.controller;

//import java.io.IOException;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.models.Adresse;
import org.eclipse.models.Client;


@WebServlet("/creationAdresse")
public class CreationAdresseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	 
		
	
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/adresse.jsp").forward(request, response);
		    }

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    
			String nom = request.getParameter("nom");
			String prenom = request.getParameter("prenom");
			String mail = request.getParameter("mail");
			String rue = request.getParameter("rue");
			String codePostal = request.getParameter("codePostal");
			String ville = request.getParameter("ville");
			String message ;
		
			
			Client client = new Client();
			client.setNom(nom);
			client.setPrenom(prenom);
			client.setMail(mail);
			Adresse adresse = new Adresse();
			adresse.setClient(client);
			adresse.setRue(rue);
			adresse.setCodePostal(codePostal);
			adresse.setVille(ville);
			request.setAttribute("adresse", adresse);
			
			
			  if (nom.trim().isEmpty()||prenom.trim().isEmpty()||mail.trim().isEmpty()|| 
						rue.trim().isEmpty()||codePostal.trim().isEmpty()||ville.trim().isEmpty()) {
		            message = "Erreur - Vous n'avez pas rempli tous les champs obligatoires. <br> <a href=\"creerClient.jsp\">Cliquez ici</a> pour acc�der au formulaire de cr�ation d'un client.";
		        } else {
		            message = "Client cr�� avec succ�s !";
		        }
			
			
			request.setAttribute("message", message);
		this.getServletContext().getRequestDispatcher("/WEB-INF/afficherAdresse.jsp").forward(request, response);
	}

}
