<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<form method ="post" action="creationAdresse">

	<div> Formulaire de création d'adresse </div>
	<div>
		<label for="nom"> Nom *</label>
		<input type="text" id="nom" name="nom" value=""/>
	</div>
	<div>
		<label for="prenom"> Prénom *</label>
		<input type=“text” id=“prenom” name="prenom" value=""/>
	</div>
	<div>
		<label for="mail"> Mail *</label>
		<input type="text" id="mail" name="mail" value=""/>
	</div>
	
	<div>
		<label for="rue"> Rue *</label>
		<input type="text" id="rue" name="rue" value=""/>
	</div>
	
	<div>
		<label for="codePostal"> CodePostal *</label>
		<input type="text" id="codePostal" name="codePostal" value=""/>
	</div>
	
	<div>
		<label for="ville"> Ville *</label>
		<input type="text" id="ville" name="ville" value=""/>
	</div>
	
	<input type="submit" value="Creer"/>
	<p class="info"> ${ message } </p>
	</form>
</body>
</html>