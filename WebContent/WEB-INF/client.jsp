<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="style.css">
<title> Creation client </title>
</head>
<body>
<h1> Formulaire de création d'un client</h1>
		<form action="creationClient" method="post">
			<table style="with: 100%">
			
			<div>
		<label for="nom"> Nom *</label>
		<input type="text" id="nom" name="nom" value=""/>
	</div>
	<div>
		<label for="prenom"> Prénom *</label>
		<input type=“text” id=“prenom” name="prenom" value=""/>
	</div>
	<div>
		<label for="mail"> Mail *</label>
		<input type="text" id="mail" name="mail" value=""/>
	</div>
			</table>
			<button type="submit">Créer</button>
			<p class="info"> ${ message } </p>
		</form>
</body>
</html>