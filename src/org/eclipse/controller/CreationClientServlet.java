package org.eclipse.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.models.Client;

@WebServlet("/creationClient")
public class CreationClientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			  

		        this.getServletContext().getRequestDispatcher( "/WEB-INF/client.jsp" ).forward( request, response );
		    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		// TODO Auto-generated method stub
//		doGet(request, response);
		 String nom = request.getParameter( "nom" );
	        String prenom = request.getParameter( "prenom" );
	        String mail = request.getParameter( "mail" );

//	        String message;
//	       
//	        if ( nom.trim().isEmpty() || prenom.trim().isEmpty() || mail.trim().isEmpty() ) {
//	            message = "Erreur - Vous n'avez pas rempli tous les champs obligatoires. <br> <a href=\"creerClient.jsp\">Cliquez ici</a> pour acc�der au formulaire de cr�ation d'un client.";
//	        } else {
//	            message = "Client cr�� avec succ�s !";
//	        }
	       
	        Client client = new Client();
	        client.setNom( nom );
	        client.setPrenom( prenom );
	        client.setMail( mail );

	        request.setAttribute( "client", client );
//	        request.setAttribute( "message", message );
		this.getServletContext().getRequestDispatcher( "/WEB-INF/afficherClient.jsp" ).forward( request, response );
	}

}
